# API Event Logger Extension

Alternative logger for default Mule Logger that you can define the JSON structure based on a predefined JSON template.

### Overview
In MuleSoft API-led architecture tracking and troubleshooting events across multiple APIs is a very tricky situation. API-Event-Logger provides configurable enriched logging with less effort.
...

### Installation
1. Clone or fork the repository and build the connector and add this dependency to your Mule application pom.xml
```
    <dependency>
        <groupId>org.flints.mule.extension</groupId>
        <artifactId>api-event-logger</artifactId>
        <version>1.0.0-SNAPSHOT</version>
        <classifier>mule-plugin</classifier>
    </dependency>
```
2. Add below dependencies to forward logs to other logging platforms over HTTP/s
```
	<dependency>
		<groupId>com.flints.mule</groupId>
		<artifactId>jsonTemplateLayout</artifactId>
        <version>1.0.3</version>
	</dependency>
```
3. Add JSON template for the logs in you classpath. Ex: EventLayoutV1.json
```
{
    "@timestamp": "${timestamp}",
    "@version": "${applicationVersion}",
    "_correlationId": "${correlationId}",
    "_eventTimestamp": "${eventTimestamp}",
    "_messageid": "${messageId}",
    "_tracepoint": "${tracepoint}",
    "application": {
        "_apiName": "${apiName}",
        "_apiVersion": "${apiVersion}",
        "_applicationName": "${applicationName}",
        "_applicationVersion": "${applicationVersion}",
        "_environment": "${environment}",
        "_region": "${region}"
    },
    "exception": {
        "errorElement": "${errorElement}",
        "errorMessage": "${errorMessage}",
        "errorType": "${errorType}",
        "exceptionClass": "${exceptionClass}",
        "exceptionMessage": "${exceptionMessage}"
    },
    "inbound": {
        "clientIp": "${clientIp}",
        "clientName": "${clientName}",
        "requestMethod": "${requestMethod}",
        "resource": "${inboundResource}",
        "resourceId": "${inboundResourceId}"
    },
    "logger": {
        "_category": "${category}",
        "_globalConfigName": "${globalConfigName}",
        "_loggerName": "${loggerName}",
        "level": "${level}"
    },
    "message": "${message}",
    "outbound": {
        "method": "${outboundMethod}",
        "status": "${requestStatus}"
    },
    "resource": {
        "resource": "${resource}",
        "resourceId": "${resourceId}"
    },
    "thread": {
        "_threadId": "${threadId}",
        "_threadName": "${thread}",
        "processorPath": "${processorPath}"
    }
}
```
4. Update your log4j2.xml with required configuration packages. Example below shows how the HTTP appender configured with JSON based API event logging in log4j2 file.
   Ref:https://docs.mulesoft.com/runtime-manager/custom-log-appender
   Ex:
```
<?xml version="1.0" encoding="utf-8"?>
<Configuration status="INFO" packages="com.mulesoft.ch.logging.appender, com.vlkan.log4j2.logstash">
	<Appenders>
		<Console name="Console" target="SYSTEM_OUT">
			<PatternLayout pattern="%-5p %d [%t] %c [%X{transactionId}]: %m%n" />
		</Console>
		<Http name="HTTP" url="${sys:logging.url}" method="POST" >
            <MuleJsonTemplateLayout templatePath="EventLayoutV1.json"/>
		</Http>
		<Log4J2CloudhubLogAppender name="CLOUDHUB"
                addressProvider="com.mulesoft.ch.logging.DefaultAggregatorAddressProvider"
                applicationContext="com.mulesoft.ch.logging.DefaultApplicationContext"
                appendRetryIntervalMs="${sys:logging.appendRetryInterval}"
                appendMaxAttempts="${sys:logging.appendMaxAttempts}"
                batchSendIntervalMs="${sys:logging.batchSendInterval}"
                batchMaxRecords="${sys:logging.batchMaxRecords}"
                memBufferMaxSize="${sys:logging.memBufferMaxSize}"
                journalMaxWriteBatchSize="${sys:logging.journalMaxBatchSize}"
                journalMaxFileSize="${sys:logging.journalMaxFileSize}"
                clientMaxPacketSize="${sys:logging.clientMaxPacketSize}"
                clientConnectTimeoutMs="${sys:logging.clientConnectTimeout}"
                clientSocketTimeoutMs="${sys:logging.clientSocketTimeout}"
                serverAddressPollIntervalMs="${sys:logging.serverAddressPollInterval}"
                serverHeartbeatSendIntervalMs="${sys:logging.serverHeartbeatSendIntervalMs}"
                statisticsPrintIntervalMs="${sys:logging.statisticsPrintIntervalMs}">

            <PatternLayout pattern="[%d{MM-dd HH:mm:ss}] %-5p %c{1} [%t]: %m%n"/>
        </Log4J2CloudhubLogAppender>
	</Appenders>
	<Loggers>

        <AsyncLogger name="org.mule.service.http.impl.service.HttpMessageLogger" level="WARN" />
        <AsyncLogger name="org.mule.service.http" level="WARN"/>
        <AsyncLogger name="org.mule.extension.http" level="WARN"/>
		<!-- Mule logger -->        
        <AsyncLogger name="org.mule.runtime.core.internal.processor.LoggerMessageProcessor" level="INFO"/>
        <AsyncLogger name="org.mule.runtime.core.internal.exception.OnErrorPropagateHandler" level="ERROR">
        	<AppenderRef ref="HTTP" />
        </AsyncLogger>
        <!--  JSON Logger -->
        <AsyncLogger name="org.flints.mule.extension.apiEventLog" level="INFO">
        	<AppenderRef ref="HTTP" />
        </AsyncLogger>
        <AsyncLogger name="org.flints.mule.extension.transform.apiEventLog" level="INFO">
        	<AppenderRef ref="HTTP" />
        </AsyncLogger>

		<AsyncRoot level="INFO">
			<AppenderRef ref="CLOUDHUB" />
			<AppenderRef ref="Console" />
		</AsyncRoot>
	</Loggers>
</Configuration>
```
