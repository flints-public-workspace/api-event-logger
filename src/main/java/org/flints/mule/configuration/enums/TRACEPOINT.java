package org.flints.mule.configuration.enums;

import java.util.HashMap;
import java.util.Map;

public enum TRACEPOINT {

    START("START"),
    BEFORE_TRANSFORM("BEFORE_TRANSFORM"),
    AFTER_TRANSFORM("AFTER_TRANSFORM"),
    BEFORE_REQUEST("BEFORE_REQUEST"),
    AFTER_REQUEST("AFTER_REQUEST"),
    FLOW("FLOW"),
    END("END"),
    EXCEPTION("EXCEPTION");
    private final String value;
    private final static Map<String, TRACEPOINT> CONSTANTS = new HashMap<String, TRACEPOINT>();

    static {
        for (TRACEPOINT c : values()) {
            CONSTANTS.put(c.value, c);
        }
    }

    private TRACEPOINT(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public String value() {
        return this.value;
    }

    public static TRACEPOINT fromValue(String value) {
        TRACEPOINT constant = CONSTANTS.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        } else {
            return constant;
        }
    }
}
