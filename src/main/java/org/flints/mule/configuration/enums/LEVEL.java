package org.flints.mule.configuration.enums;

import java.util.HashMap;
import java.util.Map;

public enum LEVEL {

    DEBUG("DEBUG"),
    TRACE("TRACE"),
    INFO("INFO"),
    WARN("WARN"),
    ERROR("ERROR");

    private final String value;
    private final static Map<String, LEVEL> CONSTANTS = new HashMap<String, LEVEL>();

    private LEVEL(String value) {
        this.value = value;
    }

    static {
        for (LEVEL c : values()) {
            CONSTANTS.put(c.value, c);
        }
    }

    public String value() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public static LEVEL fromValue(String value) {
        LEVEL constant = CONSTANTS.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        } else {
            return constant;
        }
    }
}
