package org.flints.mule.configuration.enums;


import java.util.Arrays;

/**
 * Outbound request method and operation type
 */
public enum OUTBOUND_REQUEST_TYPE {
    HTTP("HTTP-GET", "HTTP-POST", "HTTP-PUT", "HTTP-PATCH", "HTTP-UPDATE", "HTTP-DELETE"),
    Database( "DB:SELECT","DB:INSERT","DB:DELETE"),
    Other("Other");

    private OUTBOUND_REQUEST_TYPE(String... value) {
        this.value = value;
    }

    private String [] value;

    public String[] getValue() { return value; }

    @Override
    public String toString() {
        return "OUTBOUND_REQUEST_TYPE{" +
                "value=" + Arrays.toString(value) +
                '}';
    }
}
