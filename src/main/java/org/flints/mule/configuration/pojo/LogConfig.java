package org.flints.mule.configuration.pojo;

import org.flints.mule.configuration.enums.LEVEL;
import org.flints.mule.configuration.enums.TRACEPOINT;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LogConfig {
    @Parameter
    @Optional(defaultValue = "INFO")
    @Summary("Logging level/priority")
    private LEVEL level;

    @Parameter
    @Optional(defaultValue = "org.flints.mule.extension.apiEventLog")
    @Summary("If empty, default value will be from global configuration set to org.flints.mule.extension.apiEventLog")
    private String category;

    public LEVEL getLevel() { return level; }

    public void setLevel(LEVEL level) { this.level = level; }

    public String getCategory() { return category; }

    public void setCategory(String category) {
        this.category = category;
    }

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("level", this.getLevel().value());
        applicationMap.put("category", this.getCategory());
        return applicationMap;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        LogConfig logConfig = (LogConfig) o;
        return level == logConfig.level && Objects.equals(category, logConfig.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(level, category);
    }

    @Override
    public String toString() {
        return "LogConfig{" +
                "level=" + level +
                ", category='" + category + '\'' +
                '}';
    }
}
