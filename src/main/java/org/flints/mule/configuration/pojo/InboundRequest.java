package org.flints.mule.configuration.pojo;

import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.util.HashMap;
import java.util.Map;

public class InboundRequest {

    @Parameter
    @Optional(defaultValue = "#[attributes.relativePath]")
    @Summary("API RESTful inboundResource path")
    private String inboundResource;

    @Parameter
    @Optional
    @Summary("Unique inboundResource identifier from the request or the payload")
    private String inboundResourceId;

    @Parameter
    @Optional(defaultValue = "attributes.method")
    @Summary("HTTP method received")
    private String requestMethod;

    @Parameter
    @Optional(defaultValue = "#[authentication.properties.clientName]")
    @Summary("Name of the authorized client name or ID")
    private String clientName;

    @Parameter
    @Optional(defaultValue = "#[attributes.remoteAddress]")
    @Summary("Name of the authorized client IP/host name")
    private String clientIp;

    public String getClientName() { return clientName; }

    public void setClientName(String clientName) { this.clientName = clientName;}

    public String getClientIp() { return clientIp; }

    public void setClientIp(String clientIp) {this.clientIp = clientIp;}

    public String getInboundResource() {
        return inboundResource;
    }

    public void setInboundResource(String inboundResource) {
        this.inboundResource = inboundResource;
    }

    public String getInboundResourceId() {
        return inboundResourceId;
    }

    public void setInboundResourceId(String inboundResourceId) {
        this.inboundResourceId = inboundResourceId;
    }

    public String getRequestMethod() { return requestMethod; }

    public void setRequestMethod(String requestMethod) { this.requestMethod = requestMethod; }

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("clientName", this.getClientName());
        applicationMap.put("clientIp", this.getClientIp());
        applicationMap.put("inboundResource", this.getInboundResource());
        applicationMap.put("inboundResourceId", this.getInboundResourceId());
        applicationMap.put("requestMethod", this.getRequestMethod());
        return applicationMap;
    }

    @Override
    public String toString() {
        return "InboundRequest{" +
                "inboundResource='" + inboundResource + '\'' +
                ", inboundResourceId='" + inboundResourceId + '\'' +
                ", requestMethod='" + requestMethod + '\'' +
                ", clientName='" + clientName + '\'' +
                ", clientIp='" + clientIp + '\'' +
                '}';
    }
}
