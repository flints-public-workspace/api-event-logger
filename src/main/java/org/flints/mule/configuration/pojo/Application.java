package org.flints.mule.configuration.pojo;

import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.util.HashMap;
import java.util.Map;

public class Application {

    @Parameter
    @Summary("Name of the Mule application. Recommendation: This value should be based on pom.xml")
    @Example("${application.name}")
    private String applicationName;

    @Parameter
    @Summary("Mule application version. Recommendation: This value should be based on pom.xml")
    @Example("${version}")
    private String applicationVersion;

    @Parameter
    @Optional
    @Summary("Name of the API. Recommendation: This value should be based on API Manager")
    @Example("${api.name}")
    private String apiName;

    @Parameter
    @Optional
    @Summary("Version of the API. Recommendation: This value should be based on API Manager")
    @Example("${api.version}")
    private String apiVersion;

    @Parameter
    @Optional
    @Summary("Name of the region where the application is running.")
    @Example("${mule.region}")
    private String region;

    @Parameter
    @Summary("Name of the Mule Environment where the application is running.")
    @Example("${mule.env}")
    private String environment;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("applicationName", this.getApplicationName());
        applicationMap.put("applicationVersion", this.getApplicationVersion());
        applicationMap.put("apiName", this.getApiName());
        applicationMap.put("apiVersion", this.getApiVersion());
        applicationMap.put("region", this.getRegion());
        applicationMap.put("environment", this.getEnvironment());
        return applicationMap;
    }

    @Override
    public String toString() {
        return "Application{" +
                "applicationName='" + applicationName + '\'' +
                ", applicationVersion='" + applicationVersion + '\'' +
                ", apiName='" + apiName + '\'' +
                ", apiVersion='" + apiVersion + '\'' +
                ", region='" + region + '\'' +
                ", environment='" + environment + '\'' +
                '}';
    }
}
