package org.flints.mule.configuration.pojo;

import org.flints.mule.configuration.enums.OUTBOUND_REQUEST_TYPE;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.values.OfValues;

import java.util.HashMap;
import java.util.Map;

public class OutboundRequest {

    @Parameter
    private OUTBOUND_REQUEST_TYPE outbound_request_type;

    @Parameter
    @OfValues(OutboundRequestMethod.class)
    private String outboundMethod;

    private String requestStatus;

    @Parameter
    @Optional
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @DisplayName("Request summary")
    private String requestSummary;

    public OUTBOUND_REQUEST_TYPE getRequestType() { return outbound_request_type; }

    public void setRequestType(OUTBOUND_REQUEST_TYPE outbound_request_type) { this.outbound_request_type = outbound_request_type; }

    public String getOutboundMethod() { return outboundMethod; }

    public void setOutboundMethod(String outboundMethod) { this.outboundMethod = outboundMethod; }

    public String getRequestStatus() { return requestStatus; }

    public void setRequestStatus(String requestStatus) { this.requestStatus = requestStatus; }

    public String getRequestSummary() { return requestSummary; }

    public void setRequestSummary(String requestSummary) { this.requestSummary = requestSummary; }

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("outboundMethod", this.getOutboundMethod());
        applicationMap.put("requestStatus", this.getRequestStatus());
        applicationMap.put("requestSummary", this.getRequestSummary());
        return applicationMap;
    }

    @Override
    public String toString() {
        return "OutboundRequest{" +
                "outbound_request_type=" + outbound_request_type +
                ", outboundMethod='" + outboundMethod + '\'' +
                ", requestStatus='" + requestStatus + '\'' +
                ", requestSummary='" + requestSummary + '\'' +
                '}';
    }
}
