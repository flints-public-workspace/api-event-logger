package org.flints.mule.configuration.pojo;

import org.flints.mule.configuration.enums.OUTBOUND_REQUEST_TYPE;
import org.mule.runtime.api.value.Value;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.values.ValueBuilder;
import org.mule.runtime.extension.api.values.ValueProvider;
import org.mule.runtime.extension.api.values.ValueResolvingException;

import java.util.Set;
/**
 * Outbound request method
 */
public class OutboundRequestMethod implements ValueProvider{
    @Parameter
    OUTBOUND_REQUEST_TYPE outbound_request_type;

    @Override
    public Set<Value> resolve() throws ValueResolvingException {
        Set<Value> returnVal = ValueBuilder.getValuesFor(outbound_request_type.getValue());
        return returnVal;
    }

    public OUTBOUND_REQUEST_TYPE getRequestType() {
        return outbound_request_type;
    }
}
