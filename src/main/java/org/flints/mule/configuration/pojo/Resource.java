package org.flints.mule.configuration.pojo;

import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Resource {

    @Parameter
    @Optional
    @Summary("API RESTful resource path")
    @Example("#[attributes.relativePath]")
    @Placement (tab = "Resource")
    private String resource;

    @Parameter
    @Optional
    @Summary("Unique resource identifier from the request or the payload")
    @Placement (tab = "Resource")
    private String resourceId;

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("resource", this.getResource());
        applicationMap.put("resourceId", this.getResourceId());
        return applicationMap;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        Resource resource1 = (Resource) o;
        return Objects.equals(resource, resource1.resource) && Objects.equals(resourceId, resource1.resourceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resource, resourceId);
    }

    @Override
    public String toString() {
        return "Resource{" +
                "resource='" + resource + '\'' +
                ", resourceId='" + resourceId + '\'' +
                '}';
    }
}
