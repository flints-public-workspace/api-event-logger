package org.flints.mule.configuration.pojo;

import org.flints.mule.operations.APIEventLoggerOperations;
import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.RefName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.util.HashMap;
import java.util.Map;

@Operations(APIEventLoggerOperations.class)
public class GlobalSettings {

    @RefName
    private String globalConfigName;

    @Parameter
    @ParameterGroup(name = "Application and API")
    private Application application;

    @Parameter
    @Summary("If empty, default value will be set to org.flints.mule.extension.apiEventLog")
    @Placement(tab = "Logger Config")
    private LogConfig logConfig;

    @Parameter
    @Summary("Message and transaction Ids can be set globally for all operations")
    @Placement(tab = "Event Ids")
    private EventIdentifier eventIdentifier;

    @Parameter
    @Summary("Resource details set globally for all operations")
    @Placement(tab = "Resource")
    private Resource resource;

    public String getGlobalConfigName() {
        return globalConfigName;
    }

    public void setGlobalConfigName(String globalConfigName) {this.globalConfigName = globalConfigName;}

    public Application getApplication() {return application;}

    public void setApplication(Application application) {this.application = application;}

    public LogConfig getLogConfig() { return logConfig; }

    public void setLogConfig(LogConfig logConfig) {
        this.logConfig = logConfig;
    }

    public EventIdentifier getEventIdentifier() { return eventIdentifier; }

    public void setEventIdentifier(EventIdentifier eventIdentifier) { this.eventIdentifier = eventIdentifier; }

    public Resource getResource() { return resource; }

    public void setResource(Resource resource) { this.resource = resource; }

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("globalConfigName", this.getGlobalConfigName());
        applicationMap.putAll(this.getApplication().toMap());
        applicationMap.putAll(this.getLogConfig().toMap());
        applicationMap.putAll(this.getEventIdentifier().toMap());
        applicationMap.putAll(this.getResource().toMap());
        return applicationMap;
    }

    @Override
    public String toString() {
        return "GlobalSettings{" +
                "globalConfigName='" + globalConfigName + '\'' +
                ", application=" + application +
                ", logConfig=" + logConfig +
                ", eventIdentifier=" + eventIdentifier +
                ", resource=" + resource +
                '}';
    }
}
