package org.flints.mule.configuration.pojo;

import org.flints.mule.configuration.enums.TRACEPOINT;
import org.mule.runtime.extension.api.annotation.param.ConfigOverride;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;

import java.util.HashMap;
import java.util.Map;

public class Event {
    @DisplayName
    private String loggerName;

    private TRACEPOINT tracepoint;

    private String eventTimestamp;

    @Parameter
    @ConfigOverride
    @Placement(tab = "Logger Config")
    private LogConfig logConfig;

    @Parameter
    @ConfigOverride
    @Placement(tab = "Event IDs")
    private EventIdentifier eventIdentifier;

    @Parameter
    @ConfigOverride
    @Placement(tab = "Resource")
    private Resource resource;

    public String getLoggerName() {
        return loggerName;
    }

    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }

    public TRACEPOINT getTracepoint() { return tracepoint; }

    public void setTracepoint(TRACEPOINT tracepoint) { this.tracepoint = tracepoint; }

    public String getEventTimestamp() { return eventTimestamp; }

    public void setEventTimestamp(String eventTimestamp) { this.eventTimestamp = eventTimestamp; }

    public LogConfig getLogConfig() {
        return logConfig;
    }

    public void setLogConfig(LogConfig logConfig) {
        this.logConfig = logConfig;
    }

    public EventIdentifier getEventIdentifier() { return eventIdentifier; }

    public void setEventIdentifier(EventIdentifier eventIdentifier) { this.eventIdentifier = eventIdentifier; }

    public Resource getResource() { return resource; }

    public void setResource(Resource resource) { this.resource = resource; }

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("loggerName", this.getLoggerName());
        applicationMap.put("tracepoint", this.getTracepoint().toString());
        applicationMap.put("eventTimestamp", this.getEventTimestamp());
        applicationMap.putAll(this.getLogConfig().toMap());
        applicationMap.putAll(this.getEventIdentifier().toMap());
        applicationMap.putAll(this.getResource().toMap());
        return applicationMap;
    }

    @Override
    public String toString() {
        return "Event{" +
                "loggerName='" + loggerName + '\'' +
                ", tracepoint=" + tracepoint +
                ", eventTimestamp='" + eventTimestamp + '\'' +
                ", logConfig=" + logConfig +
                ", eventIdentifier=" + eventIdentifier +
                ", resource=" + resource +
                '}';
    }
}
