package org.flints.mule.configuration.pojo;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

import java.util.HashMap;
import java.util.Map;

public class ErrorEvent {
    private String errorType;
    private String errorMessage;
    private String errorElement;
    private String exceptionClass;

    @Parameter
    @Optional
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private String exceptionMessage;

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("errorType", this.getErrorType());
        applicationMap.put("errorMessage", this.getErrorMessage());
        applicationMap.put("errorElement", this.getErrorElement());
        applicationMap.put("exceptionClass", this.getExceptionClass());
        applicationMap.put("exceptionMessage", this.getExceptionMessage());
        return applicationMap;
    }

    @Override
    public String toString() {
        return "ErrorEvent{" +
                "errorType='" + errorType + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", errorElement='" + errorElement + '\'' +
                ", exceptionClass='" + exceptionClass + '\'' +
                ", exceptionMessage='" + exceptionMessage + '\'' +
                '}';
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorElement() {
        return errorElement;
    }

    public void setErrorElement(String errorElement) {
        this.errorElement = errorElement;
    }

    public String getExceptionClass() {
        return exceptionClass;
    }

    public void setExceptionClass(String exceptionClass) {
        this.exceptionClass = exceptionClass;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
}
