package org.flints.mule.configuration.pojo;

import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EventIdentifier {

    @Parameter
    @Optional(defaultValue = "#[attributes.headers.'x-correlation-id']")
    @Summary("Message transaction ID can be different when messages are split for separate functions")
    @Placement(tab = "Event Identifier")
    private String transactionId;

    @Parameter
    @Summary("Message correlation ID can be different when messages are split for separate functions")
    @Optional(defaultValue = "#[correlationId]")
    @Placement(tab = "Event Identifier")
    private String messageId;

    public String getTransactionId() { return transactionId; }

    public void setTransactionId(String transactionId) { this.transactionId = transactionId; }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Map<String, String> toMap() {
        Map<String, String> applicationMap = new HashMap<>();
        applicationMap.put("transactionId", this.getTransactionId());
        applicationMap.put("messageId", this.getMessageId());
        return applicationMap;
    }

    @Override
    public String toString() {
        return "EventIdentifier{" +
                "transactionId='" + transactionId + '\'' +
                ", messageId='" + messageId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventIdentifier that = (EventIdentifier) o;
        return Objects.equals(transactionId, that.transactionId) && Objects.equals(messageId, that.messageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, messageId);
    }
}
