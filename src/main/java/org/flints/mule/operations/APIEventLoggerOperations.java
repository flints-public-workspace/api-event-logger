package org.flints.mule.operations;

import org.apache.logging.log4j.*;
import org.flints.mule.configuration.enums.LEVEL;
import org.flints.mule.configuration.enums.TRACEPOINT;
import org.flints.mule.configuration.pojo.*;
import org.mule.runtime.api.component.location.ComponentLocation;
import org.mule.runtime.api.message.Error;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Config;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.mule.runtime.extension.api.annotation.param.MediaType.ANY;


/**
 * This class is a container for operations, every public method in this class will be taken as an extension operation.
 */
public class APIEventLoggerOperations {
    protected transient Logger eventLogger;
    private static final Logger LOGGER = LogManager.getLogger(APIEventLoggerOperations.class);

    @MediaType(value = ANY, strict = false)
    public void start(@ParameterGroup(name = "Generic") Event event, @ParameterGroup(name = "Inbound Request") InboundRequest inboundRequest, @Config GlobalSettings globalSettings) {
        event.setEventTimestamp(LocalDateTime.now().toString());
        event.setTracepoint(TRACEPOINT.START);

        Map<String, String> contextData = this.CreateCommonContextData(event, globalSettings);
        contextData.putAll(inboundRequest.toMap());
        writeEventLog(event.getLogConfig(), contextData, TRACEPOINT.START.toString());
    }

    public void beforeTransform(@ParameterGroup(name = "Generic") Event event, @Optional @Expression(ExpressionSupport.NOT_SUPPORTED) @DisplayName("Transformation summary") String transformation, @Config GlobalSettings globalSettings) {
        event.setEventTimestamp(LocalDateTime.now().toString());
        event.setTracepoint(TRACEPOINT.BEFORE_TRANSFORM);

        Map<String, String> contextData = this.CreateCommonContextData(event, globalSettings);
        contextData.put("transformation", transformation);
        writeEventLog(event.getLogConfig(), contextData, transformation != null ? transformation : "");
    }

    public void afterTransform(@ParameterGroup(name = "Generic") Event event, @Optional @Expression(ExpressionSupport.NOT_SUPPORTED) @DisplayName("Transformation summary") String transformation, @Config GlobalSettings globalSettings) {
        event.setEventTimestamp(LocalDateTime.now().toString());
        event.setTracepoint(TRACEPOINT.AFTER_TRANSFORM);

        Map<String, String> contextData = this.CreateCommonContextData(event, globalSettings);
        contextData.putAll(event.getResource().toMap());
        writeEventLog(event.getLogConfig(), contextData, transformation != null ? transformation : "");
    }

    public void beforeRequest(@ParameterGroup(name = "Generic") Event event, @ParameterGroup(name = "Request") OutboundRequest outboundRequest, @Config GlobalSettings globalSettings) {
        event.setEventTimestamp(LocalDateTime.now().toString());
        event.setTracepoint(TRACEPOINT.BEFORE_REQUEST);

        Map<String, String> contextData = this.CreateCommonContextData(event, globalSettings);
        contextData.putAll(outboundRequest.toMap());
        writeEventLog(event.getLogConfig(), contextData, outboundRequest.getRequestSummary() != null ? outboundRequest.getRequestSummary() : "");
    }

    public void afterRequest(@ParameterGroup(name = "Generic") Event event, @ParameterGroup(name = "Request") OutboundRequest outboundRequest, @Summary("Message response status") String requestStatus, @Config GlobalSettings globalSettings) {
        event.setEventTimestamp(LocalDateTime.now().toString());
        event.setTracepoint(TRACEPOINT.AFTER_REQUEST);
        outboundRequest.setRequestStatus(requestStatus);

        Map<String, String> contextData = this.CreateCommonContextData(event, globalSettings);
        contextData.putAll(outboundRequest.toMap());
        writeEventLog(event.getLogConfig(), contextData, outboundRequest.getRequestSummary() != null ? outboundRequest.getRequestSummary() : "");
    }

    public void end(@ParameterGroup(name = "Generic") Event event, @Config GlobalSettings globalSettings) {
        event.setEventTimestamp(LocalDateTime.now().toString());
        event.setTracepoint(TRACEPOINT.END);

        Map<String, String> contextData = this.CreateCommonContextData(event, globalSettings);
        writeEventLog(event.getLogConfig(), contextData, TRACEPOINT.END.toString());
    }

    public void exception(@ParameterGroup(name = "Generic") Event event, @ParameterGroup(name = "Exception") ErrorEvent errorEvent, ComponentLocation componentLocation, @Config GlobalSettings globalSettings, Error error) {
        event.setEventTimestamp(LocalDateTime.now().toString());
        event.setTracepoint(TRACEPOINT.EXCEPTION);
        LEVEL previousLevel = event.getLogConfig().getLevel();
        event.getLogConfig().setLevel(LEVEL.ERROR);

        errorEvent.setErrorType(error.getErrorType().toString());
        errorEvent.setErrorMessage(error.getDescription());
        errorEvent.setErrorElement(componentLocation.getLocation());
        errorEvent.setExceptionClass(componentLocation.getRootContainerName());

        Map<String, String> contextData = this.CreateCommonContextData(event, globalSettings);
        contextData.putAll(errorEvent.toMap());
        writeEventLog(event.getLogConfig(), contextData, TRACEPOINT.EXCEPTION.toString());
        event.getLogConfig().setLevel(previousLevel);
    }

    private void writeEventLog(LogConfig logConfig, Map<String, String> contextData, String message) {
        initLoggerCategory(logConfig.getCategory());
        contextData.values().removeIf(Objects::isNull);
        ThreadContext.putAll(contextData);
        Marker marker = MarkerManager.getMarker("Marker");
        Marker APP_NAME = MarkerManager.getMarker(contextData.get("applicationName")).setParents(marker);
        switch (logConfig.getLevel()) {
            case TRACE:
                eventLogger.trace(APP_NAME, message);
                break;
            case DEBUG:
                eventLogger.debug(APP_NAME, message);
                break;
            case INFO:
                eventLogger.info(APP_NAME, message);
                break;
            case WARN:
                eventLogger.warn(APP_NAME, message);
                break;
            case ERROR:
                eventLogger.error(APP_NAME, message);
                break;
        }
        ThreadContext.clearAll();
    }

    private Map<String, String> CreateCommonContextData(Event event, GlobalSettings globalSettings){
        Map<String, String> contextData = new HashMap<>();
        contextData.putAll(event.toMap());
        contextData.putAll(globalSettings.toMap());
        return contextData;
    }

    protected void initLoggerCategory(String category) {
        if (category != null) {
            eventLogger = LogManager.getLogger(category);
        } else {
            eventLogger = LogManager.getLogger("org.flints.mule.extension.apiEventLog");
        }
    }
}