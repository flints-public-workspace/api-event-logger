package org.flints.mule.extension;

import org.flints.mule.configuration.pojo.GlobalSettings;
import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.extension.api.annotation.Configurations;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;
import org.mule.sdk.api.annotation.JavaVersionSupport;
import org.mule.sdk.api.meta.JavaVersion;


/**
 * This is the main class of an extension, is the entry point from which configurations, connection providers, operations
 * and sources are going to be declared.
 */
@Xml(prefix = "api-event-logger")
@Extension(name = "API Event Logger")
@JavaVersionSupport({JavaVersion.JAVA_17})
@Configurations(GlobalSettings.class)
public class APIEventLoggerExtension {

}
